﻿using System;
using System.Collections.Generic;
using System.Drawing.Printing;
using System.Management;
using System.Windows.Forms;

namespace WindowsFormsApp2
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            // Find all of the installed printers.
            foreach (string printer in PrinterSettings.InstalledPrinters)
            {
                comboBox1.Items.Add(printer);
            }

            // Select the first printer.
            comboBox1.SelectedIndex = 0;
        }


        // If the data is not null and has a value, return it.
        private string GetPropertyValue(PropertyData data)
        {
            if ((data == null) || (data.Value == null)) return "";
            return data.Value.ToString();
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            
            Dictionary<UInt32, string> PrinterStatuses = new Dictionary<UInt32, string>();
            PrinterStatuses.Add(1, "Other");
            PrinterStatuses.Add(2, "Unknown");
            PrinterStatuses.Add(3, "Idle");
            PrinterStatuses.Add(4, "Printing");
            PrinterStatuses.Add(5, "Warming Up");
            PrinterStatuses.Add(6, "Stopped Printing");
            PrinterStatuses.Add(7, "Offline");


            Dictionary<UInt32, string> PrinterStates = new Dictionary<UInt32, string>();
            PrinterStates.Add(0, "Printer ready");
            PrinterStates.Add(1, "Printer paused");
            PrinterStates.Add(2, "Printer error");
            PrinterStates.Add(4, "Printer pending deletion");
            PrinterStates.Add(8, "Paper jam");
            PrinterStates.Add(16, "Out of paper");
            PrinterStates.Add(32, "Manual feed");
            PrinterStates.Add(64, "Paper problem");
            PrinterStates.Add(128, "Printer offline");
            PrinterStates.Add(256, "IO active");
            PrinterStates.Add(512, "Printer busy");
            PrinterStates.Add(1024, "Printing");
            PrinterStates.Add(2048, "Printer output bin full");
            PrinterStates.Add(4096, "Not available");
            PrinterStates.Add(8192, "Waiting");
            PrinterStates.Add(16384, "Processing");
            PrinterStates.Add(32768, "Initializing");
            PrinterStates.Add(65536, "Warming up");
            PrinterStates.Add(131072, "Toner low");
            PrinterStates.Add(262144, "No toner");
            PrinterStates.Add(524288, "Page punt");
            PrinterStates.Add(1048576, "User intervention");
            PrinterStates.Add(2097152, "Out of memory");
            PrinterStates.Add(4194304, "Door open");
            PrinterStates.Add(8388608, "Server unknown");
            PrinterStates.Add(6777216, "Power save");


            // Get a ManagementObjectSearcher for the printer.
            string query = "SELECT * FROM Win32_Printer WHERE Name='" +
                comboBox1.SelectedItem.ToString() + "'";
            ManagementObjectSearcher searcher =
                new ManagementObjectSearcher(query);

            // Get the ManagementObjectCollection representing
            // the result of the WMI query. Loop through its
            // single item. Display some of that item's properties.
            foreach (ManagementObject service in searcher.Get())
            {
                txtName.Text = service.Properties["Name"].Value.ToString();
                

                UInt32 state =(UInt32)service.Properties["PrinterState"].Value;
                txtState.Text = PrinterStates[state];

                UInt16 status = (UInt16)service.Properties["PrinterStatus"].Value;
                txtStatus.Text = PrinterStatuses[status];


                txtDescription.Text =
                    GetPropertyValue(service.Properties["Description"]);
                txtDefault.Text =
                    GetPropertyValue(service.Properties["Default"]);
                txtHorRes.Text =
                    GetPropertyValue(service.Properties["HorizontalResolution"]);
                txtVerRes.Text =
                    GetPropertyValue(service.Properties["VerticalResolution"]);
                txtPort.Text =
                    GetPropertyValue(service.Properties["PortName"]);

                lstPaperSizes.Items.Clear();
                string[] paper_sizes =
                    (string[])service.Properties["PrinterPaperNames"].Value;
                foreach (string paper_size in paper_sizes)
                {
                    lstPaperSizes.Items.Add(paper_size);
                }

                // List the available properties.
                foreach (PropertyData data in service.Properties)
                {
                    string txt = data.Name;
                    if (data.Value != null)
                        txt += ": " + data.Value.ToString();
                    Console.WriteLine(txt);
                }
            }
        }
    }
}
